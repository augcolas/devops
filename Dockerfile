FROM php:8.2-apache
RUN apt-get update && apt-get install -y apache2
COPY src/ /var/www/html/
EXPOSE 80
RUN docker-php-ext-install pdo pdo_mysql
