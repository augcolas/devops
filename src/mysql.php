<?php


$bdd_params_object = new stdClass;
$bdd_params_object->db_host = getenv('DB_HOSTNAME');
$bdd_params_object->db_name = getenv('DB_NAME');
$bdd_params_object->db_port = getenv('DB_PORT') ?: 3306;
$bdd_params_object->user = getenv('DB_USER');
$bdd_params_object->password = getenv('DB_PASSWORD');
$bdd_params_object->connexion = 'host=';
$bdd_params_object->charset = 'utf8';

try {
    // Créez une connexion sans spécifier de base de données
    $pdo = new PDO('mysql:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';charset=' . $bdd_params_object->charset, $bdd_params_object->user, $bdd_params_object->password);

    // Désactivez le mode émulation des requêtes préparées
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    // Configurez le mode d'exception PDO
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Vérifiez si la base de données existe déjà
    $databaseExists = false;
    $result = $pdo->query("SHOW DATABASES");
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        if ($row['Database'] === $bdd_params_object->db_name) {
            $databaseExists = true;
            break;
        }
    }

    if ($databaseExists) {
        echo "La base de données 'ynov-database' existe déjà.";
    } else {
        // Utilisez la requête SQL pour créer la base de données
        $sql = "CREATE DATABASE IF NOT EXISTS `{$bdd_params_object->db_name}` CHARACTER SET utf8";
        $pdo->exec($sql);

        echo "La base de données 'ynov-database' a été créée avec succès.";
    }

    // Fermez la connexion
    $pdo = null;
} catch (Exception $e) {
    echo "Erreur : " . $e->getMessage();
}

?>
